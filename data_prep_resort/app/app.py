"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import pandas as pd
import numpy as np
import os

__author__ = "### Author ###"

logger = XprLogger("data_prep_resort", level=logging.INFO)


class DataPrepResort(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, train_file_path, test_file_path):
        super().__init__(name="data_prep_resort")
        """ Initialize all the required constansts and data here """
        self.features = ['hotel', 'is_canceled', 'lead_time', 'arrival_date_year', 'arrival_date_month',
                         'arrival_date_week_number',
                         'arrival_date_day_of_month', 'stays_in_weekend_nights', 'stays_in_week_nights', 'adults',
                         'children', 'babies', 'meal',
                         'country', 'market_segment', 'distribution_channel', 'is_repeated_guest',
                         'previous_cancellations', 'previous_bookings_not_canceled', 'reserved_room_type',
                         'assigned_room_type', 'booking_changes', 'deposit_type', 'agent', 'company',
                         'days_in_waiting_list', 'customer_type', 'adr',
                         'required_car_parking_spaces', 'total_of_special_requests', 'reservation_status',
                         'reservation_status_date']
        self.Test_Columns = ['is_canceled']
        self.train_data = pd.read_csv(train_file_path)
        self.test_data = pd.read_csv(test_file_path)

    @staticmethod
    def changing_datatype(dataframe):
        if 'is_canceled' in dataframe:
            dataframe['is_canceled'] = dataframe['is_canceled'].astype('int64')
        dataframe.lead_time = dataframe.lead_time.astype('int64')
        dataframe.arrival_date_year = dataframe.arrival_date_year.astype('int64')
        dataframe.arrival_date_week_number = dataframe.arrival_date_week_number.astype('int64')
        dataframe.arrival_date_day_of_month = dataframe.arrival_date_day_of_month.astype('int64')
        dataframe.stays_in_weekend_nights = dataframe.stays_in_weekend_nights.astype('int64')
        dataframe.stays_in_week_nights = dataframe.stays_in_week_nights.astype('int64')
        dataframe.adults = dataframe.adults.astype('int64')
        dataframe.babies = dataframe.babies.astype('int64')
        dataframe.is_repeated_guest = dataframe.is_repeated_guest.astype('int64')
        dataframe.previous_cancellations = dataframe.previous_cancellations.astype('int64')
        dataframe.previous_bookings_not_canceled = dataframe.previous_bookings_not_canceled.astype('int64')
        dataframe.booking_changes = dataframe.booking_changes.astype('int64')
        dataframe.days_in_waiting_list = dataframe.days_in_waiting_list.astype('int64')
        dataframe.adr = dataframe.adr.astype('float64')
        dataframe.required_car_parking_spaces = dataframe.required_car_parking_spaces.astype('int64')
        dataframe.total_of_special_requests = dataframe.total_of_special_requests.astype('int64')
        return dataframe

    @staticmethod
    def missing_value_agent(dataframe, unique_country):
        temp = pd.DataFrame()
        for country in unique_country:
            df_filter = dataframe.loc[(dataframe['country'] == country)]
            val = df_filter['agent'].mode()[0]
            if val == 'NULL':
                result = df_filter.agent.value_counts()
                value = result.index[1]
                df_filter['agent'] = df_filter['agent'].replace('NULL', value)
            else:
                df_filter['agent'] = df_filter['agent'].replace('NULL', val)
            temp = pd.concat([temp, df_filter], axis=0)
        return temp

    @staticmethod
    def missing_value_imputation(dataframe):
        dataframe['children'] = dataframe['children'].replace('NA', 0)
        # changing the datatype of the children column to proper format
        dataframe.children = dataframe.children.fillna(0)
        dataframe.children = dataframe.children.astype('int64')
        # creating a new column total_guests
        dataframe['total_guests'] = dataframe['adults'] + dataframe['children'] + dataframe['babies']

        # dropping company column as it contains 94% missing value
        dataframe = dataframe.drop('company', axis=1)

        # imputing missing value of meal column
        val_meal = dataframe['meal'].mode()[0]
        dataframe['meal'] = dataframe['meal'].replace('Undefined', val_meal)
        # imputing missing value of country column
        val_country = dataframe['country'].mode()[0]
        dataframe['country'] = dataframe['country'].replace('NULL', val_country)

        # take top 6 values of the country column for encoding purpose
        need = dataframe['country'].value_counts().index[:6]
        dataframe['country'] = np.where(dataframe['country'].isin(need), dataframe['country'], 'OTHER')

        # unique country list of the dataframe
        unique_country = list(dataframe.country.unique())

        # missing value imputation for agent column
        dataframe = DataPrepResort.missing_value_agent(dataframe, unique_country)
        # imputing missing value for distribution channel
        val_dis_channel = dataframe['distribution_channel'].mode()[0]
        dataframe['distribution_channel'] = dataframe['distribution_channel'].replace('Undefined', val_dis_channel)
        return dataframe

    @staticmethod
    def feature_engineering(dataframe):
        dataframe = dataframe.drop('previous_cancellations', axis=1)
        dataframe = dataframe.drop(columns=['adults', 'children', 'babies', 'reserved_room_type'], axis=1)
        dataframe['total_nights_stay'] = dataframe["stays_in_weekend_nights"] + dataframe["stays_in_week_nights"]
        dataframe = dataframe.drop(['stays_in_weekend_nights', 'stays_in_week_nights', 'distribution_channel'], axis=1)
        return dataframe

    @staticmethod
    def encoding(dataframe):
        dataframe.arrival_date_year = dataframe.arrival_date_year.astype('int64')
        dataframe.arrival_date_day_of_month = dataframe.arrival_date_day_of_month.astype('int64')
        dataframe.agent = dataframe.agent.fillna(0)
        dataframe.agent = dataframe.agent.astype('int64')
        dataframe = dataframe.drop(['reservation_status', 'reservation_status_date', 'country'], axis=1)
        d = {'January': 1, 'February': 2, 'March': 3, 'April': 4, 'May': 5, 'June': 6, 'July': 7, 'August': 8,
             'September': 9, 'October': 10, 'November': 11, 'December': 12}
        dataframe.arrival_date_month = dataframe.arrival_date_month.map(d)

        # Encoding the meal column
        label_encoder = LabelEncoder()
        dataframe['meal'] = label_encoder.fit_transform(dataframe['meal'])

        # Encoding deposite_type
        dataframe['deposit_type'] = np.where(dataframe['deposit_type'] == 'Refundable',
                                             'Non Refund', dataframe['deposit_type'])
        dataframe['deposit_type'] = label_encoder.fit_transform(dataframe['deposit_type'])
        need_room = dataframe.assigned_room_type.value_counts().index[:4]
        dataframe['assigned_room_type'] = np.where(dataframe['assigned_room_type'].isin(need_room),
                                                   dataframe['assigned_room_type'], 'OTHER')
        need_segment = dataframe['market_segment'].value_counts().index[:4]
        dataframe['market_segment'] = np.where(dataframe['market_segment'].isin(need_segment),
                                               dataframe['market_segment'], 'OTHER')
        columns = ['assigned_room_type', 'market_segment', 'customer_type']
        for col in columns:
            dataframe = pd.concat([dataframe, pd.get_dummies(dataframe[col], prefix=col)], axis=1)
            dataframe.drop([col], axis=1, inplace=True)

        return dataframe

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """

        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        logging.info("Preparing data ...")
        dataframe_train = DataPrepResort.changing_datatype(self.train_data)
        dataframe_train = DataPrepResort.missing_value_imputation(dataframe_train)
        dataframe_train = DataPrepResort.feature_engineering(dataframe_train)
        dataframe_train = DataPrepResort.encoding(dataframe_train)

        dataframe_test = DataPrepResort.changing_datatype(self.test_data)
        dataframe_test = DataPrepResort.missing_value_imputation(dataframe_test)
        dataframe_test = DataPrepResort.feature_engineering(dataframe_test)
        dataframe_test = DataPrepResort.encoding(dataframe_test)

        # dataframe_combined = pd.concat([dataframe_train,dataframe_test],axis=0)
        # dataframe_combined.reset_index(inplace=True, drop=True)
        self.train_data = dataframe_train
        self.send_metrics("Shape of the training data", self.train_data)
        self.test_data = dataframe_test
        self.send_metrics("Shape of the test data", self.test_data)

        print(self.train_data.tail())
        logging.info("Completed data preparation")
        self.completed()

    def send_metrics(self, desc, data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": desc},
            "metric": {"rows": str(data.shape[0]),
                       "columns": str(data.shape[1])}
        }

        self.report_status(status=report_status)

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===

        output_dir = "/data/resort_hotel/"

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        self.train_data.to_csv(os.path.join(output_dir, "resort_hotel_train_final.csv"), index=False)
        self.test_data.to_csv(os.path.join(output_dir, "resort_hotel_test_final.csv"), index=False)

        logging.info("Data saved")
        dat = pd.read_csv(os.path.join(output_dir, "resort_hotel_train_final.csv"))
        print(dat)
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DataPrepResort(train_file_path="/data/resort_hotel/resort_hotel_train.csv",
                               test_file_path="/data/resort_hotel/resort_hotel_test_new.csv")

    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
