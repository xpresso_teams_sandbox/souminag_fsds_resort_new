"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold, cross_validate, cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
import pickle
import pandas as pd
import numpy as np
import os

__author__ = "### Author ###"

logger = XprLogger("train_resort",level=logging.INFO)


class Trainresort(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self,file_path):
        super().__init__(name="train_resort")
        """ Initialize all the required constansts and data here """
        self.Test_Columns = ['is_canceled']
        self.full_data = pd.read_csv(file_path)
        self.output_dir = "/model/resort_hotel/"

    @staticmethod
    def train_test_spliting(df):
         x_features = df.iloc[:, df.columns != "is_canceled"]
         x_labels = df.iloc[:, df.columns == "is_canceled"]
         X_train, X_test, Y_train, Y_test = train_test_split(x_features, x_labels, test_size=0.2, random_state=0)
         return X_train, X_test, Y_train, Y_test

    @staticmethod
    def calculate_matrix(cm):
        TN = cm[0, 0]
        FP = cm[0, 1]
        FN = cm[1, 0]
        TP = cm[1, 1]
        recall = TP / (TP + FN)
        precision = TP / (TP + FP)
        f1_score = (2 * recall * precision) / (recall + precision)
        return recall, precision, f1_score

    @staticmethod
    def final_model(X_train,Y_train):
        model_obj = RandomForestClassifier()
        model_obj.fit(X_train, Y_train)
        return model_obj

    @staticmethod
    def evaluation(model_obj, X_test,Y_test):
            y_pred_test = model_obj.predict(X_test)
            labels = [0, 1]
            print(f"For model accuarcy score for test data: {accuracy_score(Y_test, y_pred_test) * 100}")
            cm_test = confusion_matrix(Y_test, y_pred_test, labels)
            recall_test, precision_test, f1_score_test = Trainresort.calculate_matrix(cm_test)
            return recall_test,precision_test,f1_score_test


    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.full_data = self.full_data.drop('hotel', axis=1)
        saved_file_name = ""
        X_train,X_test,Y_train,Y_test = Trainresort.train_test_spliting(self.full_data)
        model_obj = Trainresort.final_model(X_train, Y_train)

        recall_test,precision_test,f1_score_test =Trainresort.evaluation(model_obj,X_test,Y_test)
        self.send_metrics("Precision on test data", precision_test)
        self.send_metrics("F1_score on test data", f1_score_test)
        logging.info("Model storing....")
        saved_file_name = 'resort_RF.pkl'
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        pickle.dump(model_obj, open(os.path.join(self.OUTPUT_DIR, saved_file_name), 'wb'))

        self.completed()

    def send_metrics(self,desc,value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """

        report_status = {
            "status": {"status": desc},
            "metric": {"metric_key": value}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=True,success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py


    data_prep = Trainresort(file_path="/data/resort_hotel/resort_hotel_train_final.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
